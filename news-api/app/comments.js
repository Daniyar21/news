const express = require('express');
const path = require('path');
const config = require('../config');
const mysqlDb = require('../mysqlDb');


const router = express.Router();

router.get('/', async (req, res) => {
  const comments = await mysqlDb.getConnection().query('SELECT * FROM ??', ['comments']);
  res.send(comments[0]);
});



router.delete('/:id', async (req, res) => {
  const [comment] = await mysqlDb.getConnection().query('DELETE FROM ?? where id = ?',
    ['comments', req.params.id])
  if (!comment) {
    return res.status(404).send({error: 'Comment not found'});
  } else {
    res.send({message: 'Done'});
  }
});


router.post('/', async (req, res) => {
  if (!req.body.description || !req.body.news_id) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const comment = {
    news_id: req.body.news_id,
    author: req.body.author,
    description: req.body.description
  };

  const newComment = await mysqlDb.getConnection().query('INSERT INTO ?? (news_id, author, description) values (?, ?,?)',
    ['comments', comment.news_id, comment.author, comment.description]
  );

  res.send({
    ...comment,
    id: newComment[0].insertId
  });
});

module.exports = router;
