const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const mysqlDb = require('../mysqlDb');


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});
const upload = multer({storage});
const router = express.Router();

router.get('/', async (req, res) => {
  const news = await mysqlDb.getConnection().query('SELECT id, title, image, date_of_publishing FROM ??', ['news']);
  res.send(news[0]);
});

router.get('/:id', async (req, res) => {
  const [news] = await mysqlDb.getConnection().query(`SELECT * FROM ?? where id = ?`,
    ['news', req.params.id])
  if (!news) {
    return res.status(404).send({error: 'News not found'});
  }
  res.send(news[0]);
});


router.delete('/:id', async (req, res) => {
  const [news] = await mysqlDb.getConnection().query('DELETE FROM ?? where id = ?',
    ['news', req.params.id])
  if (!news) {
    return res.status(404).send({error: 'Data not found'});
  } else {
    res.send({message: 'Done'});
  }
});

module.exports = router; // export default router;
