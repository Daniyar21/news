const express = require('express');
const cors = require('cors');
const comments = require('./app/comments');
const news = require('./app/news');
const mysqlDb = require('./mysqlDb');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;

app.use('/comments', comments);
app.use('/news', news);

mysqlDb.connect().catch(e => console.log(e));

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});
