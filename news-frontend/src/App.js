import './App.css';
import {Route, Switch} from "react-router-dom";
import News from "./containers/News/News";
import AddNew from "./containers/AddNew/AddNew";

const App = () => {

    return (
        <div className="App">
            <h1>News</h1>
            <div>
                <Switch>
                    <Route path='/' exact component={News}/>
                    <Route path='/addNew'  component={AddNew} />
                </Switch>
            </div>
        </div>
    );
};

export default App;
