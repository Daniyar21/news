import React from 'react';
import './Message.css'

const Message = ({title, description, date}) => {
    return (
        <div className='message'>
            <h1>{title}</h1>
            <p>{description}</p>
            <p>{date}</p>

            <button>Read more</button>
            <button>Delete</button>
        </div>
    );
};

export default Message;