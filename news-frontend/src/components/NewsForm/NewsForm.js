import React from 'react';
import './NewsForm.css';

const NewsForm = (props) => {
    return (
        <div className="news-block">
                <div></div>
                <div>
                    <h3>{props.title}</h3>
                    <span>Created: {props.date}</span>
                    <button>Read full post</button>
                    <button>Delete</button>
                </div>
        </div>
    );
};

export default NewsForm;