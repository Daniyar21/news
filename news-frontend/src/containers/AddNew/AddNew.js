import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {createPost} from "../../store/actions/actions";

const AddNew = ({history}) => {

    const [state, setState] = useState({
        title: "",
        description: "",
        image: null,
    });

    const dispatch = useDispatch();


    const submitHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        const onSubmit = async data => {
            await dispatch(createPost(data));
        };

        onSubmit(formData);
        history.replace('/');
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file}
        });
    };

    return (
        <div>
            <input
                placeholder='Author'
                type='text'
                name='title'
                value={state.title}
                onChange={inputChangeHandler}/>
            <p>-</p>
            <input
                placeholder='Message'
                type='text'
                name='description'
                value={state.description}
                onChange={inputChangeHandler}/>
            <p>-</p>

            <input
                type='file'
                name='image'
                onChange={fileChangeHandler}/>
            <button onClick={submitHandler}>Send</button>
        </div>
    );
};

export default AddNew;