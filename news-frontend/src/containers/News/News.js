import React, {useEffect} from 'react';
import './News.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/actions/actions";
import Message from "../../components/Message/Message";
import {Link} from "react-router-dom";

const News = () => {

    const dispatch = useDispatch();
    const posts = useSelector(state => state.news);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    return (
        <div>
            <div className='posts-header'>
                <h1>Posts</h1>
                <Link to='/addNew'>Add new post</Link>
                <div>
                {posts && posts.map(p=>(
                    <Message
                     title={p.title}
                    description={p.description}
                    date={p.date_of_publishing}/>
                ))}
                </div>
            </div>
        </div>
    );
};

export default News;