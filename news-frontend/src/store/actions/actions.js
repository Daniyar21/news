import axios from "axios";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAILURE = 'FETCH_POST_FAILURE';



export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = data => ({type: FETCH_POSTS_SUCCESS, payload: data});
export const fetchPostsFailure = () => ({type: FETCH_POSTS_FAILURE});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = () => ({type: CREATE_POST_FAILURE});

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = product => ({type: FETCH_POST_SUCCESS, payload: product});
export const fetchPostFailure = () => ({type: FETCH_POST_FAILURE});


export const fetchPosts = () => {
  return async dispatch => {
    try {
      dispatch(fetchPostsRequest());
      const response = await axios.get('http://localhost:8000/news');
      dispatch(fetchPostsSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostsFailure());
    }
  };
};

export const createPost = data => {
  return async dispatch => {

    try {
      dispatch(createPostRequest());

      await axios.post('http://localhost:8000/news', data);

      dispatch(createPostSuccess());

    } catch (e) {
      dispatch(createPostFailure());
      throw e;
    }
  };
};


export const fetchPost = id => {
  return async dispatch => {
    try {
      dispatch(fetchPostRequest());
      const response = await axios.get('http://localhost:8000/news/' + id);
      dispatch(fetchPostSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostFailure());
    }
  };
};

