drop database if exists news;
create database if not exists news;
use news;

create table if not exists news (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    description text not null,
    image varchar(255) null,
    date_of_publishing date null
);

create table if not exists comments(
    id      int          not null auto_increment primary key,
    news_id int          not null,
    author  varchar(255) null,
    description text         not null,
    constraint comments_news_id_fk
        foreign key (news_id)
            references news (id)
            on update cascade
            on delete cascade
);
insert into news (title, description, image, date_of_publishing) values
 ('Trump signed new law', 'bla bla bla', null, '2020-10-22'),
 ('New movie Avatar 2', 'not interesting movie maybe', null, '2020-09-15');

select *
from news;
